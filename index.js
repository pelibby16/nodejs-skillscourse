var express = require('express');
var app = express();

app.get('/', function (req, res) {
   res.sendFile(__dirname + '/public/index.html');
});
app.get('/my-function', myFunction);

function myFunction(req,res){
    // do something on the server-side
    // such as read a file or calculate a value
    res.send("myValue");
}

app.use(express.static(__dirname + '/public'));

var server = app.listen(8081, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://localhost%s:%s", host, port)
});